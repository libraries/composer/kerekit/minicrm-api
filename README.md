# MiniCRM API Client

Features:
- Respects rate limit and delays requests if needed.
- Provides basic `get()`, `post()` and `put()` methods any kind of requests.
- Provides a `getAll()` method for concatenating items from all pages.
- Handles authentication from constructor arguments.
- Formats requests and parses responses, throws custom error if fails.

## Installation

Install the latest version with

```bash
$ composer require kerekit/minicrm-api
```

## Basic Usage

```php
<?php

use Kerekit\MiniCrmApi\{Client,Error};

/**
 * Config:
 * - $systemId: The number in address bar, right after the "r3.minicrm.hu/" part
 * - $apiKey: You can generate one in your account's Settings > System menu
 * - $production: Whether to use production (true) or test (false) server
 */
$systemId = '1234';
$apiKey = 'IDontPasteAnActualApiKeyHereDoYouThinkIAmThatStupid';
$production = false;

// Init API client
$api = new Client ($systemId, $apiKey, $production);

// Get the first page of projects in a single module
$response = $api->get ('Project', ['CategoryId' => 19, 'Page' => 0]);
echo "Got $response[Count] projects in module. Here is the FIRST PAGE:\n";
foreach ($response ['Results'] as $project) {
    echo "- #$project[Id] $project[Name]\n";
}
echo "\n";

// Load the projects of same criteria from all pages into an array
$projects = $api->getAll ('Project', ['CategoryId' => 19]);
$count = count ($projects);
echo "Got $count projects in module. Here is ALL OF THEM:\n";
foreach ($projects as $project) {
    echo "- #$project[Id] $project[Name]\n";
}
echo "\n";

// Update project name
try {
    $api->put ('Project/123', ['Name' => 'New project name']);
    echo "Project name successfully updated.\n";
} catch (Error $e) {
    echo "Failed to update project name: " . $e->getMessage () . "\n";
}
```

See official MiniCRM API documentation for available resources and params.
