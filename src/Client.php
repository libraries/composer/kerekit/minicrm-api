<?php

namespace Kerekit\MiniCrmApi;

class Client
{
    public const API_ROOT_PROD = 'https://r3.minicrm.hu/Api/R3/';
    public const API_ROOT_TEST = 'https://r3.minicrm.hu/Api/R3/';
    public const RESULT_LIMIT = 100;
    public const MAX_REQUESTS_PER_MINUTE = 60;

    protected string $apiKey;

    /** @var float Timestamp of last request with microsecond precision */
    protected float $lastRequestStart;

    /** @var bool Using production server? */
    protected bool $prod;

    protected string $systemId;

    public function __construct (string $systemId, string $apiKey, bool $prod)
    {
        $this->systemId = $systemId;
        $this->apiKey = $apiKey;
        $this->prod = $prod;
    }

    public function get (string $path, array $params = []): array
    {
        return $this->_call ('GET', $path, $params);
    }

    public function post (string $path, array $data = []): array
    {
        return $this->_call ('POST', $path, $data);
    }

    public function put (string $path, array $data = []): array
    {
        return $this->_call ('PUT', $path, $data);
    }

    /** Returns all items from all pages */
    public function getAll (string $path, array $params = []): array
    {
        $page = 0;
        $allResults = [];
        do {
            $response = $this->get ($path, ['Page' => $page] + $params);
            $count = $response ['Count'];
            $allResults = array_merge ($allResults, $response ['Results']);
            $page ++;
        } while ($count > 0 && $count > $page * self::RESULT_LIMIT);
        return $allResults;
    }

    protected function _call (
        string $method,
        string $path,
        array $data = []
    ): array {
        // Init URL with path only
        $url = $this->prod ? self::API_ROOT_PROD : self::API_ROOT_TEST;
        $url .= $path;

        // Init request
        $request = curl_init ($url);
        curl_setopt_array ($request, [
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERPWD => "$this->systemId:$this->apiKey",
        ]);

        // Add data (to URL or payload)
        if (count ($data)) {
            switch ($method) {
                case 'GET':
                    $query = http_build_query ($data);
                    curl_setopt ($request, CURLOPT_URL, "$url?$query");
                    break;

                case 'POST':
                case 'PUT':
                    $payload = json_encode ($data);
                    curl_setopt ($request, CURLOPT_POSTFIELDS, $payload);
                    break;

                default:
                    throw new Error ("Unexpected request method: '$method'.");
                    break;
            }
        }

        // Respect rate limit
        if (isset ($this->lastRequestStart)) {
            $elapsed = microtime (true) - $this->lastRequestStart;
            $delay = 60 / self::MAX_REQUESTS_PER_MINUTE - $elapsed;
            if ($delay > 0) {
                usleep (ceil ($delay * pow (10, 6)));
            }
        }

        // Send request
        $this->lastRequestStart = microtime (true);
        $responseStr = curl_exec ($request);
        $errno = curl_errno ($request);
        $error = curl_error ($request);
        if ($errno !== CURLE_OK) {
            throw new Error ("Unexpected request error ($errno): '$error'.");
        }
        $info = curl_getinfo ($request);
        curl_close ($request);

        // Check status code
        if ($info ['http_code'] !== 200) {
            $msg = "Got a response with status $info[http_code]: '$responseStr'"
                . " (URL: $url)";
            throw new Error ($msg);
        }

        // Parse response
        $response = json_decode ($responseStr, true);
        $errno = json_last_error ();
        if ($errno !== JSON_ERROR_NONE) {
            $error = json_last_error_msg ();
            throw new Error ("Failed to parse response as JSON ($errno): '$error'.");
        }

        return $response;
    }
}
